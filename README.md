# [Start Bootstrap - SB Admin 2](https://startbootstrap.com/template-overviews/sb-admin-2/)

[SB Admin 2](https://startbootstrap.com/template-overviews/sb-admin-2/) is an open source admin dashboard theme for [Bootstrap](http://getbootstrap.com/) created by [Start Bootstrap](http://startbootstrap.com/).

I just ported it to Django.

## Preview

[![SB Admin 2 Preview](https://startbootstrap.com/assets/img/screenshots/themes/sb-admin-2.png)](https://blackrockdigital.github.io/startbootstrap-sb-admin-2/)

**[Launch Live Preview](https://blackrockdigital.github.io/startbootstrap-sb-admin-2/)**

## Download and Installation

To begin using this template, choose one of the following options to get started:

-   Clone the repo: `git clone https://github.com/ProHamad/django-sb-admin-2.git`
-   [Fork, Clone, or Download on GitHub](https://github.com/ProHamad/django-sb-admin-2)

## Bugs and Issues

Have a bug or an issue with this template? Open a new issue here on GitHub.
